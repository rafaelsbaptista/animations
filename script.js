$(document).ready(function() {
	function refresh() {
		$('.clear').remove();
		var elogios = ['gentil', 'querido', 'prestativo', 'amigo', 'inteligente', 'respons&aacute;vel', 'confi&aacute;vel', 'engra&ccedil;ado', 'criativo', 'parceiro'];
		var animations = ['bounce', 'flash', 'pulse', 'rubberBand', 'shake', 'headShake', 'swing', 'tada', 'fadeInDown', 'jello'];
		var colors = ['#000000', '#0000FF', '#ED872D', '#7F3E98', '#4B3621', '#1E4D2B', '#960018', '#66FF00', '#DE5D83', '#5F1933'];
		var  a, e, c;

		while(elogios.length > 0) {
			e = Math.floor(Math.random() * (elogios.length));
			a = Math.floor(Math.random() * (animations.length));
			c = Math.floor(Math.random() * (colors.length));

			$('#content').append('<h1 style="color:' + colors[c] + ';text-align:center;" class="' + animations[a] + ' animated clear">' + elogios[e] + '</h1>');

			elogios.splice(e, 1);
			animations.splice(a, 1);
			colors.splice(c, 1);

		}

	}

	refresh();

	$('button').on('click', function(e) {
		e.preventDefault();
		refresh();
	});
});